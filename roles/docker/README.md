Docker
=========

Installs docker, docker-compose
Defines user with key auth.

Requirements
------------

Pass `docker_user_ssh_key` as a parameter 

Role Variables
--------------

* **docker_user_ssh_key** `string` - public ssh key
* **docker_compose_version** `string` - version of docker-compose

Dependencies
------------

None

Example Playbook
----------------
```yml
- hosts: all
  roles:
    - role: docker
      vars:
        docker_compose_version: v2.4.0
        docker_user_ssh_key: key

```

License
-------

none

Author Information
------------------

Alexandr Komnatnyy @heartlessguy
