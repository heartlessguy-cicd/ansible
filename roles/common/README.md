Common
=========

Installs requested packages trough apt

Requirements
------------

None.

Role Variables
--------------

* **requested_packages** `list(string)` - list of apt packages to be installed

Dependencies
------------

None

Example Playbook
----------------
```yml
- hosts: all
  roles:
    - role: common
      vars: 
        - requested_packages: 
          - "mc"
```

License
-------

none

Author Information
------------------

Alexandr Komnatnyy @heartlessguy
