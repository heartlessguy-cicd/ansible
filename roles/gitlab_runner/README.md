Gitlab_runner
=========

Installs gitlab-runner.  
Registers it to project by given project key.

Requirements
------------

Pass `glr_registration_token` as a parameter 

Role Variables
--------------

* **glr_registration_token** - project registration token. [More info](https://docs.gitlab.com/ee/security/token_overview.html#runner-registration-tokens).
* **glr_version** - version of gitlab-runner to be installed.

*For more, see defaults/main.yml*

Dependencies
------------

None

Example Playbook
----------------
```yml
- hosts: all
  roles:
    - role: gitlab_runner
      vars:
        glr_registration_token: token
        glr_version: 14.9.1

```

License
-------

none

Author Information
------------------

Alexandr Komnatnyy @heartlessguy
