# howto
## If you are me
```bash
ansible-playbook main.yml
```
## If you are not me
1. Remove or flush `vars/secret.yml` 
2. Define `glr_project_token` as a Gitlab Runner auth token
3. Define `user_ssh_key` as public ssh string to be added to `user` user
4. ```bash
    ansible-playbook main.yml
    ```

Things that meant to me better marked as TODOs

## Notes for reviewers
1. Как и описано в TODO's, подключение раннера лучше выполнить отдельным модулем, так как факт-чекинг желательно делать через Gitlab Api. Наличие таких проверок в рольке сделает её нечитабельной как мининмум.
2. Надеюсь, ничего что я писал роли вместо plain-playbook'ов? Так проще заводить молекулу и тестить все отдельно. Кстати, молекула в таком виде, в котором она представлена тут заводится на macbook'ах с m1.
